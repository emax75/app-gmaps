import React, { useState, useRef, useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'

import PlaceInput from '../components/PlaceInput'
import { GMAP_API_KEY } from '../config'

function Home() {

    const gmap = useRef(null);

    const [origin, setOrigin] = useState(null);
    const [destination, setDestination] = useState(null);

    useEffect(() => {

        gmap.current.fitToElements(true)

    }, [origin, destination])

    return (

        <View style={styles.container}>

            <View style={styles.inputContainer}>

                <PlaceInput
                    onSelect={place => {

                        // console.log(place)

                        const location = {
                            latitude: place.result.geometry.location.lat,
                            longitude: place.result.geometry.location.lng
                        }

                        setOrigin(location)

                        // console.log(location)

                    }}
                />

                <PlaceInput
                    onSelect={place => {

                        const location = {
                            latitude: place.result.geometry.location.lat,
                            longitude: place.result.geometry.location.lng
                        }

                        setDestination(location)

                        // console.log(location)

                    }}
                />

            </View>

            <MapView
                ref={gmap}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                initialCamera={{
                    center: {
                       latitude: 7.873053999999999,
                       longitude: 80.77179699999999,
                   },
                   pitch: 0,
                   heading: 0,    
                   altitude: 10,      
                   zoom: 7
                }}
            >

                {origin && destination &&

                    <MapViewDirections
                        origin={origin}
                        destination={destination}
                        apikey={GMAP_API_KEY}
                    />

                }

                {origin &&
                    <Marker
                        coordinate={origin}
                    />
                }

                {destination &&
                    <Marker
                        coordinate={destination}
                    />
                }

            </MapView>

        </View>

    )
}

export default Home


const styles = StyleSheet.create({

    inputContainer: {
        position: 'absolute',
        width: '100%',
        paddingLeft: 10,
        paddingRight: 10,
        top: 50
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: '#000'
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    }

})
