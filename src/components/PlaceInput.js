import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import PlacesInput from 'react-native-places-input'

import { GMAP_API_KEY } from '../config'

export default function PlaceInput(props) {

    const inputProps = { ...props };

    delete(inputProps.styles)

    return (

        <View style={[styles.container, props.styles]}>

            <PlacesInput
                googleApiKey={GMAP_API_KEY}
                stylesContainer={styles.inputContainer}
                stylesInput={styles.placeInput}
                {...inputProps}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    container: {

        marginBottom: 10

    },
    inputContainer: {
        position: 'relative',
        alignSelf: 'stretch',
        margin: 0,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        shadowOpacity: 0,
        borderColor: '#dedede',
    },
    placeInput: {
        // position: 'relative'
    }
})

